﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntroducereDate.Domain.ValueTypes
{
    public class SupplierType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
