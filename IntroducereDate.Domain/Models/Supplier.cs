﻿using IntroducereDate.Domain.ValueTypes;
using System;
using System.Collections.Generic;
using System.Text;


namespace IntroducereDate.Domain.Models
{
    public class Supplier : DomainObject
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public SupplierType SupplierType { get; set; }
        public IEnumerable<Course> Courses { get; set; }
    }
}
