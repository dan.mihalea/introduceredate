﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntroducereDate.Domain.Models
{
    public class Course : DomainObject
    {
        public Supplier Supplier { get; set; }
        public string COR { get; set; }
        public int DecisionNumber { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string AuthorizationSerialNumber { get; set; }
        public int AuthorizationNumber { get; set; }
        public string Notes { get; set; }
        public string Appeal { get; set; }
        public string AppealSolutions { get; set; }
        public string ClaimedBy { get; set; }
        public IEnumerable<CourseParticipant> Participants { get; set; }
    }
}
