﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntroducereDate.Domain.Models
{
    public class CourseParticipant : DomainObject
    {

        public int CourseId { get; set; }
        public Course Course { get; set; }

        public int ParticipantId { get; set; }
        public Participant Participant { get; set; }

        public int Mark { get; set; }
    }
}
