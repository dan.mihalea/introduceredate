﻿using IntroducereDate.WPF.ViewModels;
using System.Windows;
using Forms = System.Windows.Forms;

namespace IntroducereDate.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly Forms.NotifyIcon _notifyIcon;
        public App()
        {
            _notifyIcon = new Forms.NotifyIcon();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            Window window = new MainWindow();
            //Window window = new MainWindowAcrylic();
            window.DataContext = new MainViewModel();
            window.Show();

            _notifyIcon.Icon = new System.Drawing.Icon("Assets/Icons/icon.ico");
            _notifyIcon.Text = "Introducere Date";
            _notifyIcon.Click += NotifyIcon_Click;
            _notifyIcon.Visible = true;

            base.OnStartup(e);
        }

        private void NotifyIcon_Click(object sender, System.EventArgs e)
        {
            MainWindow.WindowState = WindowState.Normal;
            MainWindow.Activate();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            _notifyIcon.Visible = false;
            _notifyIcon.Dispose();
            
            base.OnExit(e);
        }
    }
}
