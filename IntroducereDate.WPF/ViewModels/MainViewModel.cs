﻿using IntroducereDate.WPF.State.Navigators;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntroducereDate.WPF.ViewModels
{
    public class MainViewModel
    {
        public INavigator Navigator { get; set; } = new Navigator();
    }
}
