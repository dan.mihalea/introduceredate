﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntroducereDate.WPF.ViewModels
{
    public class PaginationViewModel : ViewModelBase
    {
        private int _allItems;

        public int AllItems
        {
            get { return _allItems; }
            set
            {
                _allItems = value;
                OnPropertyChanged();
            }
        }
        private int _pageSize;

        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                _pageSize = value;
                OnPropertyChanged();
            }
        }
        private int _currentPage;

        public int CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                OnPropertyChanged();
            }
        }
        private int _allPages;

        public int AllPages
        {
            get { return _allPages; }
            set
            {
                _allPages = value;
                OnPropertyChanged();
            }
        }

        public PaginationViewModel()
        {
            this.CurrentPage = 1;
            this.AllPages = 1;
        }

    }
}
