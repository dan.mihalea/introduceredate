﻿using IntroducereDate.Domain.Models;
using IntroducereDate.Domain.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroducereDate.WPF.ViewModels
{
    public class SupplierViewModel : ViewModelBase
    {
        private readonly IDataService<Supplier> _supplierDataService;

        private ObservableCollection<Supplier> _suppliers;
        public ObservableCollection<Supplier> Suppliers
        {
            get { return _suppliers; }
            set
            {
                _suppliers = value;
                OnPropertyChanged();
            }
        }

        public int DataCount
        {
            get { return _suppliers.Count(); }
        }
        public string CardName
        {
            get { return "Furnizori - nou"; }
        }

        public SupplierViewModel(IDataService<Supplier> supplierDataService)
        {
            _supplierDataService = supplierDataService;
        }

        private void LoadSuppliers()
        {
            _supplierDataService.GetAll().ContinueWith(task =>
            {
                if (task.Exception == null) Suppliers = new ObservableCollection<Supplier>(task.Result);
            });
        }

        public static SupplierViewModel LoadSupplierViewModel(IDataService<Supplier> supplierDataService)
        {
            SupplierViewModel supplierViewModel = new SupplierViewModel(supplierDataService);
            supplierViewModel.LoadSuppliers();
            return supplierViewModel;
        }


    }
}
