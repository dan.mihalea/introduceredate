﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntroducereDate.WPF.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        public SupplierViewModel SupplierViewModel { get; set; }

        public HomeViewModel(SupplierViewModel supplierViewModel)
        {
            SupplierViewModel = supplierViewModel;
        }
    }
}
