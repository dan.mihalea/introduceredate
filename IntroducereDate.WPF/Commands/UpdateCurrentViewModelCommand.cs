﻿using IntroducereDate.Domain.Models;
using IntroducereDate.Domain.Services;
using IntroducereDate.EF.Services;
using IntroducereDate.WPF.State.Navigators;
using IntroducereDate.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IntroducereDate.WPF.Commands
{
    class UpdateCurrentViewModelCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private readonly IDataService<Supplier> _supplierDataService = new GenericDataService<Supplier>(new EF.IntroducereDateDbContextFactory());

        private INavigator _navigator;

        public UpdateCurrentViewModelCommand(INavigator navigator)
        {
            _navigator = navigator;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter is ViewType)
            {
                ViewType viewType = (ViewType)parameter;
                switch (viewType)
                {
                    case ViewType.Home:
                        _navigator.CurrentViewModel = new HomeViewModel(SupplierViewModel.LoadSupplierViewModel(_supplierDataService));
                        break;
                    case ViewType.Suppliers:
                        _navigator.CurrentViewModel = SupplierViewModel.LoadSupplierViewModel(_supplierDataService);
                        break;
                    case ViewType.Courses:
                        _navigator.CurrentViewModel = new CourseViewModel();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}