﻿using IntroducereDate.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace IntroducereDate.WPF.State.Navigators
{
    public enum ViewType
    {
        Home,
        Suppliers,
        Courses
    }
    public interface INavigator
    {
        ViewModelBase CurrentViewModel { get; set; }
        ICommand UpdateCurrentViewModelCommand { get; }
    }
}
