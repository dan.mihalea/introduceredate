﻿using IntroducereDate.WPF.Commands;
using IntroducereDate.WPF.Models;
using IntroducereDate.WPF.ViewModels;
using System;
using System.ComponentModel;
using System.Windows.Input;

namespace IntroducereDate.WPF.State.Navigators
{
    public class Navigator : ObservableObject, INavigator
    {
        private ViewModelBase _currentViewModel;
        public ViewModelBase CurrentViewModel
        {
            get { return _currentViewModel; }
            set
            {
                _currentViewModel = value;
                OnPropertyChanged();
            }
        }

        public ICommand UpdateCurrentViewModelCommand => new UpdateCurrentViewModelCommand(this);


    }
}
