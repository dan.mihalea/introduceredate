﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IntroducereDate.WPF
{
    /// <summary>
    /// Interaction logic for MainWindowAcrylic.xaml
    /// </summary>
    public partial class MainWindowAcrylic : Window
    {
        public MainWindowAcrylic()
        {
            InitializeComponent();
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
