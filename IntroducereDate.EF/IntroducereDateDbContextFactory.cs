﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntroducereDate.EF
{
    public class IntroducereDateDbContextFactory : IDesignTimeDbContextFactory<IntroducereDateDbContext>
    {
        public IntroducereDateDbContext CreateDbContext(string[] args = null)
        {
            var options = new DbContextOptionsBuilder<IntroducereDateDbContext>();
            options.UseSqlite("Filename=./testDatabase.db");

            return new IntroducereDateDbContext(options.Options);
        }
    }
}
