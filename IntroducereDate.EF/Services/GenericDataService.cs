﻿using IntroducereDate.Domain.Models;
using IntroducereDate.Domain.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IntroducereDate.EF.Services
{
    public class GenericDataService<T> : IDataService<T> where T : DomainObject
    {
        private readonly IntroducereDateDbContextFactory _contextFactory;

        public GenericDataService(IntroducereDateDbContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public async Task<T> Create(T entity)
        {
            using (IntroducereDateDbContext context = _contextFactory.CreateDbContext())
            {
                EntityEntry<T> createdResult = await context.Set<T>().AddAsync(entity);
                await context.SaveChangesAsync();

                return createdResult.Entity;
            }
        }

        public async Task<bool> Delete(int id)
        {
            using (IntroducereDateDbContext context = _contextFactory.CreateDbContext())
            {
                T existingRecord = await context.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
                context.Set<T>().Remove(existingRecord);
                await context.SaveChangesAsync();

                return true;
            }
        }

        public async Task<T> Get(int id)
        {
            using (IntroducereDateDbContext context = _contextFactory.CreateDbContext())
            {
                T existingRecord = await context.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
                return existingRecord;
            }
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            using (IntroducereDateDbContext context = _contextFactory.CreateDbContext())
            {
                IEnumerable<T> existingRecords = await context.Set<T>().ToListAsync();
                return existingRecords;
            }
        }

        public async Task<T> Update(int id, T entity)
        {
            using (IntroducereDateDbContext context = _contextFactory.CreateDbContext())
            {
                entity.Id = id;
                context.Set<T>().Update(entity);
                await context.SaveChangesAsync();

                return entity;
            }
        }
    }
}
