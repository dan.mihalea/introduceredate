﻿using IntroducereDate.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace IntroducereDate.EF
{
    public class IntroducereDateDbContext : DbContext
    {
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Participant> Participants { get; set; }

        public IntroducereDateDbContext(DbContextOptions options) : base(options)
        {
            //Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CourseParticipant>()
                .HasKey(cp => new { cp.CourseId, cp.ParticipantId });

            modelBuilder.Entity<CourseParticipant>()
                .HasOne(cp => cp.Course)
                .WithMany(cp => cp.Participants)
                .HasForeignKey(cp => cp.CourseId);
            modelBuilder.Entity<CourseParticipant>()
                .HasOne(cp => cp.Participant)
                .WithMany(cp => cp.Courses)
                .HasForeignKey(cp => cp.ParticipantId);

        }

    }
}
