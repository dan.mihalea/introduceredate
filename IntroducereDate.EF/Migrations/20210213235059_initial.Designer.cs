﻿// <auto-generated />
using System;
using IntroducereDate.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace IntroducereDate.EF.Migrations
{
    [DbContext(typeof(IntroducereDateDbContext))]
    [Migration("20210213235059_initial")]
    partial class initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.3");

            modelBuilder.Entity("IntroducereDate.Domain.Models.Course", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Appeal")
                        .HasColumnType("TEXT");

                    b.Property<string>("AppealSolutions")
                        .HasColumnType("TEXT");

                    b.Property<int>("AuthorizationNumber")
                        .HasColumnType("INTEGER");

                    b.Property<string>("AuthorizationSerialNumber")
                        .HasColumnType("TEXT");

                    b.Property<string>("COR")
                        .HasColumnType("TEXT");

                    b.Property<string>("ClaimedBy")
                        .HasColumnType("TEXT");

                    b.Property<int>("DecisionNumber")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Notes")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("ReleaseDate")
                        .HasColumnType("TEXT");

                    b.Property<int?>("SupplierId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("SupplierId");

                    b.ToTable("Courses");
                });

            modelBuilder.Entity("IntroducereDate.Domain.Models.CourseParticipant", b =>
                {
                    b.Property<int>("CourseId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("ParticipantId")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Id")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Mark")
                        .HasColumnType("INTEGER");

                    b.HasKey("CourseId", "ParticipantId");

                    b.HasIndex("ParticipantId");

                    b.ToTable("CourseParticipant");
                });

            modelBuilder.Entity("IntroducereDate.Domain.Models.Participant", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Email")
                        .HasColumnType("TEXT");

                    b.Property<string>("FirstName")
                        .HasColumnType("TEXT");

                    b.Property<string>("LastName")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Participants");
                });

            modelBuilder.Entity("IntroducereDate.Domain.Models.Supplier", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Address")
                        .HasColumnType("TEXT");

                    b.Property<string>("Email")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("TEXT");

                    b.Property<int?>("SupplierTypeId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("SupplierTypeId");

                    b.ToTable("Suppliers");
                });

            modelBuilder.Entity("IntroducereDate.Domain.ValueTypes.SupplierType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("SupplierType");
                });

            modelBuilder.Entity("IntroducereDate.Domain.Models.Course", b =>
                {
                    b.HasOne("IntroducereDate.Domain.Models.Supplier", "Supplier")
                        .WithMany("Courses")
                        .HasForeignKey("SupplierId");

                    b.Navigation("Supplier");
                });

            modelBuilder.Entity("IntroducereDate.Domain.Models.CourseParticipant", b =>
                {
                    b.HasOne("IntroducereDate.Domain.Models.Course", "Course")
                        .WithMany("Participants")
                        .HasForeignKey("CourseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("IntroducereDate.Domain.Models.Participant", "Participant")
                        .WithMany("Courses")
                        .HasForeignKey("ParticipantId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Course");

                    b.Navigation("Participant");
                });

            modelBuilder.Entity("IntroducereDate.Domain.Models.Supplier", b =>
                {
                    b.HasOne("IntroducereDate.Domain.ValueTypes.SupplierType", "SupplierType")
                        .WithMany()
                        .HasForeignKey("SupplierTypeId");

                    b.Navigation("SupplierType");
                });

            modelBuilder.Entity("IntroducereDate.Domain.Models.Course", b =>
                {
                    b.Navigation("Participants");
                });

            modelBuilder.Entity("IntroducereDate.Domain.Models.Participant", b =>
                {
                    b.Navigation("Courses");
                });

            modelBuilder.Entity("IntroducereDate.Domain.Models.Supplier", b =>
                {
                    b.Navigation("Courses");
                });
#pragma warning restore 612, 618
        }
    }
}
